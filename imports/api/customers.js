import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Customers = new Mongo.Collection('customers');

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('customers', function tasksPublication() {
    return Customers.find({});
  });
}

Meteor.methods({
  'customers.insert'(fname, lname, address, telephone) {
    check(fname, String);
    check(lname, String);
    check(address, String);
    check(telephone, String);

    // Make sure the user is logged in before inserting a customer
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    Customers.insert({
      fname:fname,
      lname:lname,
      address:address,
      telephone:telephone,
      createdAt: new Date(),
      owner: this.userId,
      username: Meteor.users.findOne(this.userId).username,
    });
  },
  'customers.remove'(customerId) {
      console.log(customerId);
    check(customerId, String);

    const customer = Customers.findOne(customerId);
    Customers.remove(customerId);
  },
  'customers.update'(customerId, fname, lname,address, telephone) {
    check(fname, String);
    check(lname, String);
    check(address, String);
    check(telephone, String);
    // const customer = Customers.findOne(customerId);
    // if (customer.private && customer.owner !== this.userId) {
    //   // If the task is private, make sure only the owner can check it off
    //   throw new Meteor.Error('not-authorized');
    // }
    console.log(customerId);
    Customers.update(customerId, { $set: {
        fname:fname,
        lname:lname,
        address:address,
        telephone:telephone } });
  }
});
