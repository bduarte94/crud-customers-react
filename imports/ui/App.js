import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import { Customers } from '../api/customers.js';
import CustomersList from './CustomersList.js';
import Form from './Form.js'

import AccountsUIWrapper from './AccountsUIWrapper.js';

// App component - represents the whole app
class App extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onHandleEdit = this.onHandleEdit.bind(this);
    //this.handleInputChange = this.handleInputChange.bind(this);
    this.state = {
                    id:'',
                    fname: '',
                    lname: '',
                    address: '',
                    telephone: ''
                 };
  }

  handleSubmit(formState) {
    const {id, fname, lname, address, telephone} = formState;
    //Execute a method async way Meteor.Call(name, arg1, arg2 ...)
    console.log ('ID: ' + id);
    if (typeof id =='undefined' || id === ''){
        Meteor.call('customers.insert', fname, lname, address, telephone);
    }else{
        Meteor.call('customers.update', id, fname, lname, address, telephone);
        //this.setState({id:''});
    }
    this.setState ({
                    id:'',
                    fname: '',
                    lname: '',
                    address: '',
                    telephone: ''
                 });
  }
  renderCustomers() {
    let filteredCustomers = this.props.customers;
    return filteredCustomers.map((customer) => {
      const currentUserId = this.props.currentUser && this.props.currentUser._id;
      const showPrivateButton = customer.owner === currentUserId;
      return (
        <CustomersList
          key={customer._id}
          customer={customer}
          showPrivateButton={showPrivateButton}
          onEdit = {this.onHandleEdit}
        />
      );
    });
  }

  onHandleEdit(customer){
       const {_id,fname,lname,address,telephone} = customer;
       this.setState({
           id:_id,
           fname,
           lname,
           address,
           telephone
       });
   }
  renderForm(){
      return (
          <Form handleSubmit = {this.handleSubmit} data = {this.state}/>
      );
  }

  render() {
    return (
      <div className="container">
        <header>
          <h1>Customers List {this.props.currentUser ? '('+this.props.incompleteCount+')' : ''}</h1>
          <AccountsUIWrapper />
          { this.props.currentUser ?
            this.renderForm() : ''
          }
        </header>
        { this.props.currentUser ?
            <ul>
              {this.renderCustomers()}
            </ul> : ''
        }

      </div>
    );
  }
}

export default withTracker(() => {
  Meteor.subscribe('customers');
  return {
    customers: Customers.find({}, { sort: { createdAt: -1 } }).fetch(),
    incompleteCount: Customers.find({}).count(),
    currentUser: Meteor.user(),
  };
})(App);
