import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';

import { Customers } from '../api/customers.js';

class CustomersList extends Component{
    constructor(){
        super();
        this.onSetData = this.onSetData.bind(this);
        this.deleteThisCustomer = this.deleteThisCustomer.bind(this);
        this.onclickExpandUL = this.onclickExpandUL.bind(this);
    }

    deleteThisCustomer() {
      Meteor.call('customers.remove', this.props.customer._id);
    }
        onclickExpandUL(event){
             for (let elem of event.currentTarget.parentElement.getElementsByClassName("Expanded")) {
                 elem.style.display = elem.style.display ==='none'?'inline':'none';
             }
        }

        onSetData(){
            this.props.onEdit(this.props.customer);
        }


    render (){
        return (
        <li>
          <button className="delete" onClick={this.deleteThisCustomer}>
            <i className="fas fa-window-close"></i>
          </button>
          <button className="delete" onClick={this.onSetData}>
            <span className="fas fa-edit"></span>
          </button>
          <button style={{float:"left"}} onClick = {this.onclickExpandUL}>
            <i className="fas fa-chevron-down"></i>
          </button>
          <span className="text">
            <strong>{this.props.customer.fname} {this.props.customer.lname}</strong>
          </span>
          <ul className = "Expanded" style={{display:"none"}}>
                <li>
                    <span>{this.props.customer.address}</span>
                </li>
                <li>
                    <span>{this.props.customer.telephone}</span>
                </li>
          </ul>
        </li>);
    }
}

export default CustomersList;
