import React, {Component} from 'react'

class Form extends Component{
    constructor(){
        super();
        this.handleInputChange = this.handleInputChange.bind(this);
        this.submit = this.submit.bind(this);
        this.state = {
                        id:'',
                        fname: '',
                        lname: '',
                        address: '',
                        telephone: ''
                     };
    }
    handleInputChange(e){
        const name = e.target.name;
        const value = e.target.value.trim();
        this.setState({[name]:value});
    }
    submit(e){
        e.preventDefault();
        this.props.handleSubmit(this.state);
    }

    UNSAFE_componentWillReceiveProps(nextProps){
        const {id, fname, lname, address, telephone} = this.props.data;
        if(nextProps.id!==id && nextProps.fname !== fname && nextProps.lname !== lname
            && nextProps.address!==address && nextProps.telephone!==telephone){
            this.setState({...nextProps.data});
        }
    }

    render(){
        return (
        <form className="new-task" onSubmit={this.submit} >
          <input
            type="text"
            name="fname"
            placeholder="Type to add First Name"
            onChange = {this.handleInputChange}
            value = {this.state.fname}
           />
          <input
            type="text"
            name="lname"
            placeholder="Type to add Last Name"
            onChange = {this.handleInputChange}
            value = {this.state.lname}
          />
          <input
            type="text"
            name="address"
            placeholder="Type to add Address"
            onChange = {this.handleInputChange}
            value = {this.state.address}
          />
          <input
            type="text"
            name="telephone"
            placeholder="Type to add Telephone"
            onChange = {this.handleInputChange}
            value = {this.state.telephone}
          />
          <button type="submit">Save</button>
        </form>
        );
    }
}
export default Form
